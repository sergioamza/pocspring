package co.com.smz.poc.model;

import java.util.Arrays;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Cliente {
	@Id
	private String identificacion;
	@Column
	private String apellido;
	@Column
	private String nombres;
	@Column
	private String ciudad_nacimiento;
	@Column
	private int edad;
	@Column
	private String tipo_identificacion;
	@Column(length=1000000)
	private String foto;
	
	public Cliente() {
		super();
	}
	public Cliente(String identificacion, String apellido, String nombres, String ciudad_nacimiento, int edad,
			String tipo_identificacion, String foto) {
		super();
		this.identificacion = identificacion;
		this.apellido = apellido;
		this.nombres = nombres;
		this.ciudad_nacimiento = ciudad_nacimiento;
		this.edad = edad;
		this.tipo_identificacion = tipo_identificacion;
		this.foto = foto;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCiudad_nacimiento() {
		return ciudad_nacimiento;
	}
	public void setCiudad_nacimiento(String ciudad_nacimiento) {
		this.ciudad_nacimiento = ciudad_nacimiento;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	public String getTipo_identificacion() {
		return tipo_identificacion;
	}
	public void setTipo_identificacion(String tipo_identificacion) {
		this.tipo_identificacion = tipo_identificacion;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	@Override
	public String toString() {
		return "Cliente [identificacion=" + identificacion + ", apellido=" + apellido + ", nombres=" + nombres
				+ ", ciudad_nacimiento=" + ciudad_nacimiento + ", edad=" + edad + ", tipo_identificacion="
				+ tipo_identificacion + ", foto=" + foto + "]";
	}
	
	
}
