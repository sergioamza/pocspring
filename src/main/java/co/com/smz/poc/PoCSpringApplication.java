package co.com.smz.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoCSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoCSpringApplication.class, args);
	}

}
