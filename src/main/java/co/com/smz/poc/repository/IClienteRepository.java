package co.com.smz.poc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.smz.poc.model.Cliente;

@Repository
public interface IClienteRepository extends JpaRepository<Cliente, String> {

}
