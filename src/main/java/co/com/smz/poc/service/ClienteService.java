package co.com.smz.poc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.smz.poc.model.Cliente;
import co.com.smz.poc.repository.IClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private IClienteRepository clienteRepository;
	
	public List<Cliente> listar()	{
		return clienteRepository.findAll();
	}
	
	public Optional<Cliente> encontrarPorId(String clienteId)	{
		return clienteRepository.findById(clienteId);
	}
	
	public Cliente insertar(Cliente cliente)	{
		System.out.println(cliente.toString());
		return clienteRepository.save(cliente);
	}
	
	public Cliente actualizar(Cliente cliente)	{
		System.out.println(cliente.toString());
		return clienteRepository.save(cliente);
	}
	
	public Cliente actualizar(String clienteId, Cliente cliente)	{
		System.out.println(cliente.toString());
		cliente.setIdentificacion(clienteId);
		System.out.println(cliente.toString());
		return clienteRepository.save(cliente);
	}
	
	public void eliminar(Cliente cliente)	{
		System.out.println(cliente.toString());
		clienteRepository.delete(cliente);
	}

	public void eliminar(String clienteId)	{
		System.out.println(clienteId);
		clienteRepository.deleteById(clienteId);
	}
}
