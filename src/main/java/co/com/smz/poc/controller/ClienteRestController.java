package co.com.smz.poc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.smz.poc.model.Cliente;
import co.com.smz.poc.service.ClienteService;

@RestController
@RequestMapping("/api/clientes")
public class ClienteRestController {
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	public List<Cliente> listar()	{
		return clienteService.listar();
	}
	
	@GetMapping(path = "/{id}")
	public Optional<Cliente> encontrarPorId(@PathVariable("id") String clienteId)	{
		return clienteService.encontrarPorId(clienteId);
	}
	
	@PostMapping
	public Cliente insertar(@RequestBody Cliente cliente)	{
		return clienteService.insertar(cliente);
	}
	
	@PutMapping
	public Cliente actualizar(@RequestBody Cliente cliente)	{
		return clienteService.insertar(cliente);
	}
	
	@PutMapping(path = "/{id}")
	public Cliente actualizar(@PathVariable("id") String clienteId, @RequestBody Cliente cliente)	{
		return clienteService.actualizar(clienteId, cliente);
	}
	
	@DeleteMapping
	public void eliminar(@RequestBody Cliente cliente)	{
		clienteService.eliminar(cliente);
	}

	@DeleteMapping(path = "/{id}")
	public void eliminar(@PathVariable("id") String clienteId)	{
		clienteService.eliminar(clienteId);
	}
}
